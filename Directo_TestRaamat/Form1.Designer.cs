﻿namespace Directo_TestRaamat
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btn_ComputeOrder = new System.Windows.Forms.Button();
            this.tb_output = new System.Windows.Forms.TextBox();
            this.num_ofPages = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.num_ofPages)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(64, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Lehekülgede arv:";
            // 
            // btn_ComputeOrder
            // 
            this.btn_ComputeOrder.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn_ComputeOrder.Location = new System.Drawing.Point(67, 95);
            this.btn_ComputeOrder.Name = "btn_ComputeOrder";
            this.btn_ComputeOrder.Size = new System.Drawing.Size(299, 40);
            this.btn_ComputeOrder.TabIndex = 2;
            this.btn_ComputeOrder.Text = "Arvuta lehekülgede järjekord";
            this.btn_ComputeOrder.UseVisualStyleBackColor = false;
            this.btn_ComputeOrder.Click += new System.EventHandler(this.btn_ComputeOrder_Click);
            // 
            // tb_output
            // 
            this.tb_output.BackColor = System.Drawing.Color.White;
            this.tb_output.Location = new System.Drawing.Point(67, 153);
            this.tb_output.Multiline = true;
            this.tb_output.Name = "tb_output";
            this.tb_output.ReadOnly = true;
            this.tb_output.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_output.Size = new System.Drawing.Size(299, 216);
            this.tb_output.TabIndex = 3;
            // 
            // num_ofPages
            // 
            this.num_ofPages.Location = new System.Drawing.Point(160, 51);
            this.num_ofPages.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.num_ofPages.Name = "num_ofPages";
            this.num_ofPages.Size = new System.Drawing.Size(120, 20);
            this.num_ofPages.TabIndex = 4;
            this.num_ofPages.Value = new decimal(new int[] {
            16,
            0,
            0,
            0});
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(441, 381);
            this.Controls.Add(this.num_ofPages);
            this.Controls.Add(this.tb_output);
            this.Controls.Add(this.btn_ComputeOrder);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.num_ofPages)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_ComputeOrder;
        private System.Windows.Forms.TextBox tb_output;
        private System.Windows.Forms.NumericUpDown num_ofPages;
    }
}

