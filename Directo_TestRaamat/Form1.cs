﻿using DirectoPageOrder;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Directo_TestRaamat
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_ComputeOrder_Click(object sender, EventArgs e)
        {
            int nrOfPages = (int)this.num_ofPages.Value;
            string pagesOrder = PageOrder.getPageOrder(nrOfPages);
            this.tb_output.Text = pagesOrder;
        }
    }
}
