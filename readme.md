## TYPE
.NET Windows Forms app

## TASK
Find correct order for page numbers, 
1. when the numbers are  printed on  e.g. A4 sheet on 4 quarters on both sides 
2. and then cut on half 
3. and then folded as described in a figure here[](https://bitbucket.org/Mailis/pagenumbersonsheet/src/master/TaskFigure.png) 
https://bitbucket.org/Mailis/pagenumbersonsheet/src/master/TaskFigure.png .

## FILE LOCATIONS 
desktop app: TestRaamatuLk\Directo_TestRaamat\bin\Debug\Directo_TestRaamat.exe
logic file: TestRaamatuLk\DirectoPageOrder\PageOrder.cs
unit tests: TestRaamatuLk\PageOrderUnitTestProject