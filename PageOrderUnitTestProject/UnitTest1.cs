﻿using System;
using DirectoPageOrder;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PageOrderUnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestThatPagesAmountIsDividableBy8()
        {
            int pagesNr = 9;
            int corrected = PageOrder.setCorrectNumberOfPages(pagesNr);
            Assert.AreEqual(0, corrected % 8);
        }

        [TestMethod]
        public void TestThatPagesOrderIsCorrect_8()
        {
            int pagesNr = 8;
            string ordered = PageOrder.getPageOrder(pagesNr);
            Assert.AreEqual("4,5,2,7,6,3,8,1", ordered);
        }

        [TestMethod]
        public void TestThatPagesOrderIsCorrect_16()
        {
            int pagesNr = 16;
            string ordered = PageOrder.getPageOrder(pagesNr);
            Assert.AreEqual("8,9,4,13,10,7,14,3,6,11,2,15,12,5,16,1", ordered);
        }

        [TestMethod]
        public void TestThatPagesOrderIsCorrect_11()
        {
            int pagesNr = 11;
            string ordered = PageOrder.getPageOrder(pagesNr);
            Assert.AreEqual("8,9,4,x,10,7,x,3,6,11,2,x,x,5,x,1", ordered);
        }

        [TestMethod]
        public void TestThatPagesOrderIsCorrect_0()
        {
            int pagesNr = 0;
            string ordered = PageOrder.getPageOrder(pagesNr);
            Assert.AreEqual("", ordered);
        }
    }
}
