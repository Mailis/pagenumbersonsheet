﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DirectoPageOrder
{
    public class PageOrder
    {
        public static string getPageOrder(int nrOfPages)
        {
            if (nrOfPages == 0)
            {
                return "";
            }
            int correctNumberOfPages = setCorrectNumberOfPages(nrOfPages);
            int addAmountToBottom = correctNumberOfPages / 4;
            List<Int32> orderedPageNumbers = orderPageNumbers(correctNumberOfPages, addAmountToBottom);
            StringBuilder sb = new StringBuilder();
            foreach (int n in orderedPageNumbers)
            {
                if (n > nrOfPages)
                {
                    sb.Append('x');
                }
                else
                {
                    sb.Append(n);
                }

                if (n != orderedPageNumbers.Last())
                {
                    sb.Append(',');
                }
            }
            return sb.ToString();
        }

        /*
         * @pageNumbers: int[] ; all page numbers in numeric order
         * @addAmountToBottom: int ; amount to compute other numbers than a number on top left corner on tha same A4 page
         * @ returns orderedPages: List<Int32>; contains ordered numbers for book
         */
        private static List<Int32> orderPageNumbers(int nrOfPages, int addAmountToBottom)
        {
            List<Int32> orderedPages = new List<Int32>();
            int front_left_top = nrOfPages / 2;
            int addAmount = 1;
            computeOneA4PageValues(front_left_top, addAmountToBottom, addAmount, orderedPages);
            return orderedPages;
        }

        /*
         * computes page numbers for one A4 page for both, front and back side
         * @leftTopPageNumber: int ; starting number at top left corner
         * @addAmount: int ; number to add to  top left corner in order to get top right corner number
         * @addAmountToBottom: int ; amount to get other numbers
         * @orderedPages: List<Int32> ; accumulator list, contains ordered numbers for book
         * 
         */
        private static void computeOneA4PageValues(int leftTopPageNumber, int addAmountToBottom, int addAmount, List<Int32> orderedPages)
        {
            int front_right_top = leftTopPageNumber + addAmount;
            int front_bottom_left = leftTopPageNumber - addAmountToBottom;
            int front_bottom_right = front_right_top + addAmountToBottom;

            int back_left_top = front_right_top + 1;
            int back_right_top = leftTopPageNumber - 1;
            int back_bottom_left = back_left_top + addAmountToBottom;
            int back_bottom_right = back_right_top - addAmountToBottom;

            orderedPages.Add(leftTopPageNumber);
            orderedPages.Add(front_right_top);
            orderedPages.Add(front_bottom_left);
            orderedPages.Add(front_bottom_right);
            orderedPages.Add(back_left_top);
            orderedPages.Add(back_right_top);
            orderedPages.Add(back_bottom_left);
            orderedPages.Add(back_bottom_right);

            if (back_bottom_right > 1)
            {
                int nextTopLeftNumberAtA4 = leftTopPageNumber - 2;
                addAmount += 4;
                computeOneA4PageValues(nextTopLeftNumberAtA4, addAmountToBottom, addAmount, orderedPages);
            }
        }

        /*
         * @nrOfPages: int ; user input
         * @ returns amount of page numbers that divides exactly with 8
         * 
         */
        public static int setCorrectNumberOfPages(int nrOfPages)
        {
            //number of pages must be divided by 8 without reminder
            if (nrOfPages % 8 != 0)
            {
                //for optimizing, instead of adding +1, after this we can add +2 in while loop
                if (nrOfPages % 2 != 0)
                {
                    nrOfPages++;
                }

                while (nrOfPages % 8 != 0)
                {
                    nrOfPages += 2;
                }
            }
            return nrOfPages;
        }
    }
}
